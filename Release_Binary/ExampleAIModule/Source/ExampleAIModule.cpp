#include "ExampleAIModule.h"
#include <iostream>

using namespace BWAPI;
using namespace Filter;

void ExampleAIModule::onStart()
{
  // Hello World!
  Broodwar->sendText("Hello world!");

  // Print the map name.
  // BWAPI returns std::string when retrieving a string, don't forget to add .c_str() when printing!
  Broodwar << "The map is " << Broodwar->mapName() << "!" << std::endl;

  // Enable the UserInput flag, which allows us to control the bot and type messages.
  Broodwar->enableFlag(Flag::UserInput);

  // Uncomment the following line and the bot will know about everything through the fog of war (cheat).
  //Broodwar->enableFlag(Flag::CompleteMapInformation);

  // Set the command optimization level so that common commands can be grouped
  // and reduce the bot's APM (Actions Per Minute).
  Broodwar->setCommandOptimizationLevel(2);

  // Check if this is a replay
  if ( Broodwar->isReplay() )
  {

    // Announce the players in the replay
    Broodwar << "The following players are in this replay:" << std::endl;

    // Iterate all the players in the game using a std:: iterator
    Playerset players = Broodwar->getPlayers();
    for(auto p : players)
    {
      // Only print the player if they are not an observer
      if ( !p->isObserver() )
        Broodwar << p->getName() << ", playing as " << p->getRace() << std::endl;
    }

  }
  else // if this is not a replay
  {
    // Retrieve you and your enemy's races. enemy() will just return the first enemy.
    // If you wish to deal with multiple enemies then you must use enemies().
    if ( Broodwar->enemy() ) // First make sure there is an enemy
      Broodwar << "The matchup is " << Broodwar->self()->getRace() << " vs " << Broodwar->enemy()->getRace() << std::endl;
  }

}

void ExampleAIModule::onEnd(bool isWinner)
{
  // Called when the game ends
  if ( isWinner )
  {
    // Log your win here!
  }
}

bool needControl()
{
  // Return if the game is a replay or is paused
  if ( Broodwar->isReplay() || Broodwar->isPaused() || !Broodwar->self() )
    return false;

  // Prevent spamming by only running our onFrame once every number of latency frames.
  // Latency frames are the number of frames before commands are processed.
  if ( Broodwar->getFrameCount() % Broodwar->getLatencyFrames() != 0 )
    return false;
  
  return true;
}

bool unitOutOfOrder(BWAPI::Unit u)
{
    // Ignore the unit if it no longer exists
    // Ignore the unit if it has one of the following status ailments
    // Ignore the unit if it is in one of the following states
    // Ignore the unit if it is incomplete or busy constructing
    if ( !u->exists() ||
            ( u->isLockedDown() || u->isMaelstrommed() || u->isStasised() ) ||
            ( u->isLoaded() || !u->isPowered() || u->isStuck() ) ||
            ( !u->isCompleted() || u->isConstructing() ))
      return true;
    else
      return false;

}

void ManageWorker(BWAPI::Unit u)
{
    if ( u->getType().isWorker() )
    {
      // if our worker is idle
      if ( u->isIdle() )
      {
        // Order workers carrying a resource to return them to the center,
        // otherwise find a mineral patch to harvest.
        if ( u->isCarryingGas() || u->isCarryingMinerals() )
        {
          u->returnCargo();
        }
        else if ( !u->getPowerUp() )  // The worker cannot harvest anything if it
        {                             // is carrying a powerup such as a flag
          // Harvest from the nearest mineral patch or gas refinery
          if ( !u->gather( u->getClosestUnit( IsMineralField || IsRefinery )) )
          {
            // If the call fails, then print the last error message
            Broodwar << Broodwar->getLastError() << std::endl;
          }

        } // closure: has no powerup
      } // closure: if idle
    }
}

void ManageSupply(Unit u)
{
   UnitType supplyProviderType = u->getType().getRace().getSupplyProvider();
   static int lastChecked = 0;
   int prodRate = Broodwar->self()->allUnitCount( UnitTypes::Enum::Terran_Barracks );
 
   if ( Broodwar->self()->supplyTotal() - Broodwar->self()->supplyUsed() < 4*(3 + prodRate) &&       // if we're supply blocked and haven't built anything recently
           lastChecked + 100 < Broodwar->getFrameCount() &&
           Broodwar->self()->incompleteUnitCount(supplyProviderType) == 0 &&
           Broodwar->self()->minerals() > 75)
      {
        Broodwar->drawTextScreen(200, 120, "Need supply");
        lastChecked = Broodwar->getFrameCount();

        // Retrieve a unit that is capable of constructing the supply needed
        Unit supplyBuilder = u->getClosestUnit(  GetType == supplyProviderType.whatBuilds().first &&
                                                    (IsIdle || IsGatheringMinerals) &&
                                                    IsOwned);
        // If a unit was found
        if ( supplyBuilder )
        {
          if ( supplyProviderType.isBuilding() )
          {
            TilePosition targetBuildLocation = Broodwar->getBuildLocation(supplyProviderType, supplyBuilder->getTilePosition());
            if ( targetBuildLocation )
            {
              // Order the builder to construct the supply structure
              supplyBuilder->build( supplyProviderType, targetBuildLocation );
            }
          }
          else
          {
            // Train the supply provider (Overlord) if the provider is not a structure
            supplyBuilder->train( supplyProviderType );
          }
        }
      }
}

void ManageBase(Unit u)
{
    static int lastChecked = 0;
    if ( u->getType().isResourceDepot() ) // A resource depot is a Command Center, Nexus, or Hatchery
    {
      // Order the depot to construct more workers! But only when it is idle.
      if ( u->isIdle() && Broodwar->self()->supplyUsed() < 40) 
      {
        u->train(u->getType().getRace().getWorker());
      } // closure: failed to train idle unit
      UnitType barracks = UnitType( UnitTypes::Enum::Terran_Barracks );
      if(Broodwar->self()->supplyUsed() >= 40 &&
              Broodwar->self()->allUnitCount( UnitTypes::Enum::Terran_Barracks ) < 4 &&
              lastChecked + 100 < Broodwar->getFrameCount() ) {
          lastChecked = Broodwar->getFrameCount();
        Unit barracksBuilder = u->getClosestUnit( GetType == barracks.whatBuilds().first &&
                                                   (IsIdle || IsGatheringMinerals) &&
                                                   IsOwned);
        if( barracksBuilder ) {
          TilePosition targetBuildLocation = Broodwar->getBuildLocation( barracks, barracksBuilder->getTilePosition());
          if( targetBuildLocation ) {
            barracksBuilder->build( barracks, targetBuildLocation );
          }
        }  
      }
    }
}

void BuildMarines(Unit u)
{
    if (u->getType().isSuccessorOf( UnitType( UnitTypes::Enum::Terran_Barracks )) &&
            u->isIdle()) {
      u->build( UnitType( UnitTypes::Enum::Terran_Marine));

    }
}

void ManageBattle(Unit u)
{
    static UnitType marine = UnitType( UnitTypes::Enum::Terran_Marine);
    int elapsedTime = Broodwar->elapsedTime() / (60 * 40);
    BWAPI::Position pos = (BWAPI::Position) Broodwar->getStartLocations()[elapsedTime % 4];

    if(u->isIdle() && u->getType() == marine)
    {
        u->attack(pos, false);
    }
}

void ExampleAIModule::onFrame()
{
  // Called once every game frame

  // Display the game frame rate as text in the upper left area of the screen
  Broodwar->drawTextScreen(200, 0,  "FPS: %d", Broodwar->getFPS() );
  Broodwar->drawTextScreen(200, 20, "Average FPS: %f", Broodwar->getAverageFPS() );
  Broodwar->drawTextScreen(200, 40, "Minerals: %d", Broodwar->self()->minerals());
  Broodwar->drawTextScreen(200, 60, "Supply used: %d", Broodwar->self()->supplyUsed());

  if(!needControl()) return;

  // Iterate through all the units that we own
  for (auto &u : Broodwar->self()->getUnits())
  {
    if ( unitOutOfOrder(u)) continue;
    
    ManageWorker(u);
    ManageSupply(u);
    ManageBase(u);
    BuildMarines(u);
    ManageBattle(u);
  } // closure: unit iterator
}

void ExampleAIModule::onSendText(std::string text)
{

  // Send the text to the game if it is not being processed.
  Broodwar->sendText("%s", text.c_str());


  // Make sure to use %s and pass the text as a parameter,
  // otherwise you may run into problems when you use the %(percent) character!

}

void ExampleAIModule::onReceiveText(BWAPI::Player player, std::string text)
{
  // Parse the received text
  Broodwar << player->getName() << " said \"" << text << "\"" << std::endl;
}

void ExampleAIModule::onPlayerLeft(BWAPI::Player player)
{
  // Interact verbally with the other players in the game by
  // announcing that the other player has left.
  Broodwar->sendText("Goodbye %s!", player->getName().c_str());
}

void ExampleAIModule::onNukeDetect(BWAPI::Position target)
{

  // Check if the target is a valid position
  if ( target )
  {
    // if so, print the location of the nuclear strike target
    Broodwar << "Nuclear Launch Detected at " << target << std::endl;
  }
  else
  {
    // Otherwise, ask other players where the nuke is!
    Broodwar->sendText("Where's the nuke?");
  }

  // You can also retrieve all the nuclear missile targets using Broodwar->getNukeDots()!
}

void ExampleAIModule::onUnitDiscover(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitEvade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitShow(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitHide(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitCreate(BWAPI::Unit unit)
{
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s creates a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
}

void ExampleAIModule::onUnitDestroy(BWAPI::Unit unit)
{
}

void ExampleAIModule::onUnitMorph(BWAPI::Unit unit)
{
  if ( Broodwar->isReplay() )
  {
    // if we are in a replay, then we will print out the build order of the structures
    if ( unit->getType().isBuilding() && !unit->getPlayer()->isNeutral() )
    {
      int seconds = Broodwar->getFrameCount()/24;
      int minutes = seconds/60;
      seconds %= 60;
      Broodwar->sendText("%.2d:%.2d: %s morphs a %s", minutes, seconds, unit->getPlayer()->getName().c_str(), unit->getType().c_str());
    }
  }
}

void ExampleAIModule::onUnitRenegade(BWAPI::Unit unit)
{
}

void ExampleAIModule::onSaveGame(std::string gameName)
{
  Broodwar << "The game was saved to \"" << gameName << "\"" << std::endl;
}

void ExampleAIModule::onUnitComplete(BWAPI::Unit unit)
{
}
